;;; qw.el --- macros like perl qw operator  -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20240613
;; Updated: 20240613
;; Version: 0.0
;; Keywords: qw, quote strings

;;; Commentary:

;; Provides string quoting macro similar to Perl's qw operator.

;;; Change Log:

;;; Code:




(defun qw-list (words)
  "Return copy of WORDS with symbols and numbers converted to strings.
Acts recursively on elements of WORDS which are themselves lists."
  (cl-typecase words
    (null      nil)
    (stringp   words)
    (integerp (format "%d" words))
    (numberp  (format "%g" words))
    (symbolp  (symbol-name words))
    (t
     (cons
      (qw-list (car words))
      (qw-list (cdr words))
      ))))

(defmacro qw (&rest words)
  "Like Perl qw, but works on nested lists as well
Example:  (qw fred ted) --> (\"fred\" \"ted\"),
or (qw a b (c d) z) --> (\"a\" \"b\" (\"c\" \"d\") \"z\")
"
  `(qw-list ',words))



(defun qw-shallow-element (elem)
  "Like `qw-list' but assumes ELEM is from a shallow list.
In other words that ELEM is not itself a list."
  (cl-etypecase elem
    (null      nil)
    (stringp   elem)
    (integerp (format "%d" elem))
    (numberp  (format "%g" elem))
    (symbolp  (symbol-name elem))
    (listp    (error "expected nonlist, but got '%S'" elem))
    ))

(defun qw-shallow-list (list)
  "Return qw quoted copy of shallow LIST."
  (mapcar 'qw-shallow-element list)
  )


(provide 'qw)

;;; qw.el ends here
