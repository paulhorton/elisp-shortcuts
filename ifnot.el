;;; ifnot.el ---  if variant  -*- lexical-binding: t -*-

;; Copyright (C) 2020,2023. Paul Horton.

;; License: GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231020
;; Updated: 20241224
;; Version: 0.0
;; Keywords: if, flow control

;;; Commentary:

;;; Change Log:

;;; Code:


(defmacro ifnot (condition then else1 &rest else-body)
  "Shortcut for (if (not CONDITION)) THEN ELSE...)
At least one ELSE expression must be provided,
otherwise use `unless' instead.

\(fn CONDITION THEN ELSE...)"
  (declare (indent 2)
           (debug (form form &rest form)))
  `(if (not ,condition) ,then ,else1 ,@else-body)
  )



(provide 'ifnot)

;;; ifnot.el ends here
