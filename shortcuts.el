;;; shortcuts.el --- shortcut programming utils  -*- lexical-binding: t; -*-

;; Copyright (C) 2020,2023. Paul Horton.

;; License: GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20200121
;; Updated: 20241009
;; Version: 0.0
;; Keywords:

;;; Commentary:

;; Naming Conventions:

;;  Function names ending in ? indicate predicates
;;
;;  Where a predicate is a function whose return
;;  value connotes true or false.
;;
;;  When feasible, the true values try to preserve information;
;;  so for example compare:
;;
;;    (not (string= "cat" "dog"))  -->   t
;;    (string≠ "cat" "dog")        --> "dog"
;;
;;  Both are true values, but string≠ tells
;;  us the string which differed from the preceding string.

;;  unicode math comparison characters.
;;  In shortcuts, names are generally restricted to ascii.
;;  However the universally recognized math characters {≦,≧,≠} are use here
;;  and in shortcuts-math-symbols-basic.
;;
;;  See shortcuts-libre, shortcuts-libre-math for less inhibited
;;  use of non-ascii characters in names.


;; NOT-TODO LIST:
;;
;; - Define: char-before-pos, char-after-pos
;; `char-before' and `char-after' already optionally take a position.


;;; Change Log:

;;; Code:

(require 'cl-format)
(require 's)
(require 'let1)
(require 'ifnot)
(require 'shortcuts-math-symbols-basic)
(require 'looking)
(require 'buffer-substring-star)



(defmacro defalias-new (target-symbol definition-symbol &optional docstring)
  "Define TARGET-SYMBOL to be an alias of DEFINITION-SYMBOL;
but signal an error if TARGET-SYMBOL already has another function value."
  `(if (symbol-function ,target-symbol)
       (or (eq ,definition-symbol (symbol-function ,target-symbol))
           (error "defalias-new; %s alias of %S already."
                  (symbol-name ,target-symbol)
                  (symbol-function ,target-symbol)
                  ))
     (defalias ,target-symbol ,definition-symbol ,docstring)
     ;; Note, no reason to provide a default docstring.
     ;; Better to leave it nil; `describe-function' will use the parent docstring.
     ))

;; To Consider:
;; Using this `defalias-new' for the shortcuts below might be a way to alleviate the cost of
;; possible name collisions with future versions of emacs or with packages (like this one) which
;; do not respect the name-prefix convention.

;; For example, the macro : below could be defined with a longer name
;; such as (defmacro shortcuts/ternary...)
;; less likely to cause name collisions.
;; and then (defalias-new ': 'shortcuts/ternary)
;; used to give it a short name to use.

(defmacro : (condition then else)
  ;; Named after C ternary operator ?:
  ":  If with exactly one THEN and one ELSE expression

Unlike `if', indents THEN and ELSE equally."
  (declare
   (debug (form form form))
   )
  `(if ,condition ,then ,else)
  )


(defalias 'cl-dbind  'cl-destructuring-bind)

(defalias-new 'region-beg  'region-beginning)

;; To Consider; region-string to return region as a string.
;; One question would be whether/how to handle rectangles.


(defalias 'deleted-region  'delete-and-extract-region)

(defalias 'match-beg  'match-beginning)

(defalias 'rxq  'regexp-quote)



(defalias 'buffer-substring-no-props  'buffer-substring-no-properties)
(defalias 'match-string-no-props      'match-string-no-properties)
(defalias 'substring-no-props         'substring-no-properties)


(defun buffer-string-no-props (&optional buffer)
  "Return (possibly narrowed) contents of BUFFER with no text properties.

BUFFER defaults to current buffer."
  (if (not buffer)
      (buffer-substring-no-properties (point-min) (point-max))
    (with-current-buffer buffer
      (buffer-substring-no-properties (point-min) (point-max))
      )))

(defun buffer-substring-safe (beg end)
  "Normally the same as `buffer-substring',
but simply returns the empty string when `buffer-substring'
would signal an error"
  (let (
        (bg (max beg (point-min)))
        (ed (min end (point-max)))
        )
    (if (<= end beg)
        ""
      (buffer-substring bg ed)
      )))

(defun buffer-substr-of-len (beg len)
  "Like `buffer-substring' but takes substring length LEN as arg,
instead of and END position.
"
  ;; Can reduce code clutter when BEG is a long expression.
  (buffer-substring beg (+ beg len))
  )

(cl-defun peek (pos &key (l 7) d)
  "Return string of buffer content around position POS.
Returned string includes content of up to L characters before
and after POS.

Drop properties if D is true.
"
  ;; short name given so it can be typed conveniently,
  ;; for example, when using the debugger.

  ;; To consider. When called with a buffer which has been narrowed
  ;; Temporarily `widen' to peek at pos,
  ;; but somehow indicate that POS not in the narrowed range
  (interactive
   (list
    (eval-minibuffer "Position sexp: ")
    ))
  (let1 peek-string (peek/string pos l d)
    (if (called-interactively-p 'any)
        (message peek-string)
      peek-string
      )))


(defun peek/string (pos length drop-props?)
  "Return string to be used by `peek', which see."
  ;; short name given so it can be typed conveniently,
  ;; for example, when using the debugger

  ;; To consider. When called with a buffer which has been narrowed
  ;; Temporarily `widen' to peek at pos,
  ;; but somehow indicate that POS not in the narrowed range
  (cl-check-type pos integer-or-marker)
  (cond
   ((< pos (point-min))
        "%d chars before point-min" (- (point-min) pos)
        )
   ((< (point-max) pos)
    "%d chars after point-max" (- pos (point-max))
    )
   (t
    (buffer-substring*
     (max (point-min) (- pos length))
     (min (point-max) (+ pos length))
     drop-props?
     ))
   ))




(defalias 'move-to-bol 'move-beginning-of-line)
(defalias 'move-to-eol 'move-end-of-line)


(cl-defun bol-pos (&optional (pos (point)))
  "Return the BOL position of line containing position POS;
BOL means Beginning Of Line.

Note: when the POS arg is omitted, the behavior is the same as
`pos-bol' builtin for emacs version 29+
Better to use that when possible.
"
  (declare (side-effect-free t))
  (save-excursion
    (goto-char pos)
    (let1 inhibit-field-text-motion t
      (line-beginning-position)
    )))

(defun pos-bol? (pos)
  "Is POS at an beg of line?
If so POS is returned, otherwise nil"
  (save-excursion
    (goto-char pos)
    (and (bolp) pos)
    ))

(cl-defun eol-pos (&optional (pos (point)))
  "Return the EOL position of line containing position POS;
EOL means End Of Line.

Note: when the POS arg is omitted, the behavior is the same as
`pos-eol' builtin for emacs version 29+
Better to use that when possible.
"
  (declare (side-effect-free t))
  (save-excursion
    (goto-char pos)
    (let1 inhibit-field-text-motion t
      (line-end-position)
    )))



;; ───────────────  Predicates  ───────────────
;;
;; Generally they accept any type, simply returning nil
;; if the argument does not have the right type and value
;;
;; When true, these functions pass their arguments through
;; rather than simply returning t, so they can be used
;; with functions such as seq-filter.
;;
(defun 0? (arg)
  "If ARG is = to 0 return 0,
Otherwise return nil."
  (and (numberp arg) (zerop arg) 0)
  )

(defun -? (arg)
  "Is ARG a negative number?

Unlike `cl-minusp'
If ARG is not a number, silently return nil."
  (and (numberp arg) (< arg 0) arg)
  )

(defun atom? (arg)
  "Like `atom' but when possible ARG is returned."
  (or (null arg)
      (and (atom arg) arg)
      ))


(defun nonnil-symbol? (thing)
  "Return THING if it is a non-nil symbol,
otherwise return false."
  (declare (side-effect-free t))
  (and (symbolp thing) thing)
  )

(defun nonnil-symbol-p (thing)
  "Same as `nonnil-symbol?'"
  ;; Name for cl-check-type
  (declare (side-effect-free t))
  (and (symbolp thing) thing)
  )


(defun mutable-symbol? (arg)
  "Is ARG a mutable symbol?
If so, return it; otherwise nil"
    (and
     (symbolp arg)
     (not (eq t arg))
     (/= ?: (aref (symbol-name arg) 0))
     arg
    ))

(defun cons? (arg)
  "If ARG is a cons cell, return it;
Otherwise return nil."
  (and (consp arg) arg)
  )

(defun dotted-pair? (thing)
  "If THING is a \"natural\" dotted-pair, return it;
otherwise return nil.

Here \"natural\" dotted pair excludes a singleton list
such as:  (a)  technically equivalent to  (a . nil)
but more \"naturally\" viewed as a list than a dotted pair.

See also `proper-list-p'
"
  (and (cdr-safe thing)
       (atom (cdr thing))
       thing
  ))

(defun dotted-list? (thing)
  "If THING is a \"natural\" dotted-list, return it;
otherwise return nil.

Here \"natural\" dotted list excludes a singleton list
such as:  (a)  technically equivalent to  (a . nil)
but more \"naturally\" viewed as simply a list, rather than a dotted list."
  ;; Modified from Drew Adams Apr 4 2015, emacs.SX answer
  (let1  last-link  (last thing)
    (and
     (cdr-safe last-link)
     (atom (cdr last-link))
     thing
     )))


(defun natnum? (arg)
  "If ARG is natural number return it,
otherwise return nil.
The natural numbers are 1,2,...
"
  (and (integerp arg) (< 0 arg) arg)
  )

(defun number? (arg)
  "If ARG is a number, return it;
otherwise nil."
  (and (numberp arg) arg)
  )

(defun number-or-marker? (arg)
  "If ARG is a number or marker, return it.
Otherwise return nil."
  (and (number-or-marker-p arg) arg)
  )

(defun cons-or-vector? (arg)
  "If ARG is a cons cell or vector, return it.
Otherwise return nil"
  (and (or (consp arg)
           (vectorp arg)
           )
       arg
       ))

(defun wholenum? (arg)
  "If ARG a whole number, pass it through,
otherwise return nil.
Whole numbers are {0} union the counting numbers.

Unlike `wholenump' negative numbers are not included."
  (and (integerp arg) (<= 0 arg) arg)
  )


;; ───────────────  More Filtering Predicates  ───────────────

(defun char? (arg)
  "If ARG is a character, return it,
otherwise return nil."
  (and (characterp arg) arg)
  )

(defun string? (arg)
  "If ARG is a character, return it,
otherwise return nil."
  (and (stringp arg) arg)
  )


(defun eq? (arg1 arg2)
  "Like `eq';
but usually return ARG1 (or equivalently ARG2)
when (eq ARG1 ARG2);
except (eq? nil nil) returns true."
  (when (eq arg1 arg2)
    (or arg1 t)
    ))

(defmacro neq (arg1 arg2)
  "Shortcut for (not (eq ...))."
  `(not (eq ,arg1 ,arg2))
  )

(cl-defun neq? (arg1 arg2 &rest more-args)
  "Return nil if all args are `eq',
Otherwise return a true value.
"
  (dolist (arg (cons arg2 more-args))
    (if (not (eq arg1 arg))
        (cl-return-from neq? (or arg t))
      )
  nil
  ))


(defun <? (number-or-marker &rest number-or-markers)
  "Like `<', but when true returns the NUMBER-OR-MARKER."
  (if (apply #'< number-or-marker number-or-markers)
      number-or-marker
    nil
    ))

(defun >? (number-or-marker &rest number-or-markers)
  "Like `>', but when true returns the NUMBER-OR-MARKER."
  (if (apply #'> number-or-marker number-or-markers)
      number-or-marker
    nil
    ))


(defun shortcuts/true-not-t? (arg)
  "If ARG is true, but not t, return it;
Otherwise return nil."
  ;; Use case:
  ;; Suppose a function argument FOLD-CASE-TABLE can have values meaning:
  ;;   * nil          -- don't fold case
  ;;   * t            -- fold case using `current-case-table'
  ;;   * a case table -- fold case using that case table
  ;;
  ;; (when FOLD-CASE-TABLE
  ;;   (with-case-table (or (true-not-t? FOLD-CASE-TABLE) (current-case-table))
  ;;     (
  ;;      ...
  ;;      )))
  (if (eq t arg) nil arg)
  )

(defalias-new 'true-not-t? 'shortcuts/true-not-t?)


(defalias 'string-list? 'c-string-list-p
  "True if ARG is a list and all of its elements are strings.
Note that register rectangles are lists of strings."
  )

(defun t? (arg)
  "is ARG the symbol t?"
  (eq t arg)
  )

(defun not0? (number)
  "If NUMBER is 0, return nil
Otherwise return it as is.
May give error if NUMBER holds a non-numerical value"
  (if (= 0 number) nil number)
  )

(defun empty? (seq)
  "Return true iff sequence SEQ is empty.
Constant time even if SEQ is a list."
  (or (null seq) (= 0 (length seq)))
  )

(defun nempty? (seq)
  "If SEQ is non-empty, return it (a true value),
otherwise return nil"
  (if (or (null seq) (= 0 (length seq)))
      nil
    seq))

(defun len=1? (seq)
  "If SEQ length is equal to one, return SEQ; otherwise nil"
  (and
   (if (listp seq)
       (and
        (consp seq)
        (not (cdr seq))
        )
     (= 1 (length seq))
    )
   seq
   ))


(defmacro unequal? (obj1 obj2)
  ;; To consider: accept an arbitray number of objs and return the first
  ;; unequal one (except t if it is nil)
    "If a and b are equal, return nil; else return a true value.
Currently the return value is (or OBJ2 t).
Assuming O
  (unequal?  'a 'a) -->  nil
  (unequal?  'a 'b) -->  'b
  (unequal?  'a  t) -->  t
  (unequal?  3 nil) -->  t
"
    (if (equal obj1 obj2)
        nil
      (or obj2 t)
      ))


(defun =? (s1 s2)
  "Like `=', but returns S1 when =."
  (and (= s1 s2) s1)
  )

(defun string=? (s1 s2)
  "Like `string=', but returns S1 when equal."
  (and (string= s1 s2) s1)
  )

(defun string≠ (s1 s2)
  "If strings S1 and S2 are equal return nil, else return S1"
  ;; For consistency, maybe should be named string≠?
  ;; To consider: accept an arbitray number of objs and return the first
  ;; unequal one (except t if it is nil)
  (if (string= s1 s2)
      nil
    s2
    ))


(defun atom1 (atom-or-list)
  "get an atom from ATOM-OR-LIST;
If necessary use car to get at an atom"
  (if (consp atom-or-list)
      (atom1 (car atom-or-list))
    atom-or-list
    ))



;; Logic/Flow control.
(defmacro and1 (condition1 &rest more-conditions)
  "Like `and', but the returns first when all are true.
For example
  (and1 'apple 'pear)
returns 'apple, not 'pear

condition1 is evaluated first, if it is true,
`and' is used to evaluate more-conditions
Requires at least one argument."
  (declare
   (indent 1)
   (debug (body))
   )
  (let ((res1 (make-symbol "res1")))
    `(let ((,res1 ,condition1))
       (and ,@more-conditions ,res1)
      )))



(defun string/elisp-reads-as-number? (s &optional leading-ws-ok?)
  "If elisp would read all of string S as a number, return that number;
otherwise false.

With LEADING-WS-OK? some whitespace in S is allowed before the number part."
  ;; See stackexchange answer by Drew Jul, 2018
  ;; https://emacs.stackexchange.com/questions/42480/simple-way-to-test-if-string-contains-float-eg-8-3
  (when (or
         leading-ws-ok?
         (string-match-p "^[-0-9.eE]" s)
         )
    (let1  object.idx  (read-from-string s)
      (and (= (cdr object.idx) (length s))
           (number? (car object.idx))
           ))))

(defalias 'looks-like-number? 'string/elisp-reads-as-number?
  "Name similar to Perl looks_like_number"
  )

;; Currently I seldom use anaphoric macros,
;; but anaphoric when seems useful.
;;
;; If I were to use more anaphoric macros,
;; Use the anaphoric package by Roland Walker,
;; would probably be better than defining them here.
(defmacro _when (condit &rest body)
  "Anaphoric when from On Lisp by P. Graham.
but uses _ instead of \"it\"."
  (declare (debug when)
           (indent 1)
           )
  `(let ((_ ,condit))
     (when _ ,@body)
     ))


(defmacro funcall: (condition fun1 fun2 &rest args)
  "Depending on CONDITION, apply FUN1 or FUN2 to ARGS."
  (declare
   (indent 3)
   (debug (form form form body))
   )
  `(funcall (if ,condition ,fun1 ,fun2) ,@args)
  )

(defmacro if-call (condition fun1 fun2 &rest args)
  "Tentatively scheduled for deprecation 20240531
use `funcall:' instead."
  (declare (indent 3))
  `(funcall (if ,condition ,fun1 ,fun2) ,@args)
  )

(defmacro funcall2< (fun arg1 arg2)
  "Call function FUN with the smaller arg first."
  ;; todo: Look for a standard name for this.
  `(if (< ,arg2 ,arg1)
       (funcall ,fun ,arg2 ,arg1)
     (funcall ,fun ,arg1 ,arg2)
     ))


(defmacro pop-if (predicate list)
  ;; macro so that it can modify LIST for the caller.
  ;; Implementation not mature, currently most just a placeholder.
  "If list has a first element that PREDICATE accepts, then `pop' it off the list.
Currently LIST must be an ordinary list variable (e.g. a symbol).
In the future, generalized variables could be allowed if there is demand that."
  (cl-assert (symbolp list) t "pop-if: expected LIST to be a symbol")
  `(and ,list
        (funcall ,predicate (car ,list))
        (pop ,list)
        ))

;; to consider: Anaphoric version (defmacro pop-aif (condition list)...)


(defmacro setq1-oldval (sym &optional place)
  "Set symbol SYM to PLACE, but return old value of SYM.
VAL defaults to nil"
  (declare
   (debug (form form))
   )
  `(prog1 ,sym
     (setf ,sym ,place)
     ))


(defmacro save-match-data-excursion (&rest body)
  "Shortcut for combining `save-excursion' and `save-match-data'"
  (declare (indent 0) (debug t))
  `(save-excursion
     (save-match-data ,@body)
    ))

(defmacro save-point-unless (&rest body)
  "Evalute BODY and return its result,
If it returns nil, restore point first.

current buffer is not restored."
  (declare
   (debug (body))
   )
  (let1 point-ori-var (gensym)
    `(let1 ,point-ori-var (point)
       (or (progn ,@body)
           (progn
             (goto-char ,point-ori-var)
             nil
             )))))


(defun shortcuts/the-region-string (&optional drop-props?)
  "Contents of the region as a string;
Including text-properties, unless drop-props? is true."
  (buffer-substring* (region-end) (region-beg) drop-props?)
  )

(defalias-new 'the-region-string 'shortcuts/the-region-string)


(defun replace-region-text (beg end newtext)
  "Replace content of region BEG END with NEWTEXT.
NEWTEXT should be a string or character.

See also `replace-region-contents'."
  (save-excursion
    (goto-char beg)
    (delete-region beg end)
    (insert newtext)
    ))

(cl-defun delete-match (&optional (subexp-arg1 0) &rest subexp-extra-args)
  "Use match data to delete the match or SUBEXP of the match in current buffer."
  (replace-match "" nil t nil subexp-arg1)
  (dolist (subexp subexp-extra-args)
    (replace-match "" nil t nil subexp)
    ))


(defun buffer-or-name/name (buffer-or-name)
  "Return name of BUFFER-OR-NAME.

When BUFFER-OR-NAME is a string, it is returned as is;
without checking if a buffer with that name exists."
  (cl-etypecase buffer-or-name
    (string buffer-or-name)
    (buffer (buffer-name buffer-or-name))
    ))

(defun window-buffer-name (window)
  "Return buffer name of window WINDOW."
  (buffer-name (window-buffer window))
  )

(defun window-buffer-filename (window)
  "Return buffer filename of window WINDOW."
  (buffer-file-name (window-buffer window))
  )

(defun current-buffer-window ()
  "Return window of current buffer.
Just calls `get-buffer-window' with no arguments."
  (get-buffer-window)
  )

(defun current-buffer-name ()
  "Return name of current buffer"
  (buffer-name (current-buffer))
  )

(defmacro with-current-buffer* (buffer-or-nil &rest body)
  "Like `with-current-buffer',
but just acts like `progn' when BUFFER-OR-NIL is nil.
The effect being to treat nil as indicating the current buffer."
  ;; Several builtin emacs commands (e.g. match-string, get-text-property)
  ;; have a optional buffer or buffer-or-string arguments
  ;; for which a value of nil indicates to use the current buffer.
  (declare
   (debug t)
   (indent 1)
   )
  `(if ,buffer-or-nil
       (with-current-buffer ,buffer-or-nil
         ,@body
         )
     (progn
       ,@body
       )))


(defun switch-to-buffer-create (buffer-or-name)
  "Switch to buffer BUFFER-OR-NAME create it if needed.
Returns the buffer.

But maybe better to use `pop-to-buffer'?"
  ;; I am wondering it it may always be best to use
  ;; `pop-to-buffer' or other builtins instead of using wrapping `switch-to-buffer'
  ;; See Stefan Monnier post
  ;; https://emacs.stackexchange.com/questions/27712/switch-to-buffer-vs-pop-to-buffer-same-window
  (switch-to-buffer (get-buffer-create buffer-or-name))
  )

(defun visiting-file-filter (buf-or-window)
  "If buffer or window BUF-OR-WINDOW is visiting a file, return buf-or-window,
otherwise return nil."
  (when
      (buffer-file-name
       (if (windowp buf-or-window) (window-buffer buf-or-window) buf-or-window)
       )
    buf-or-window
    ))

(defun make-marker-at (pos)
  "Return marker at position POS."
  (set-marker (make-marker) pos)
  )



(defun shortcuts/as-string (string-wannabe)
  "If STRING-WANNABE is a string, pass it through as is,
otherwise try to convert STRING-WANNABE to a string.
Note that integers are interpreted as characters when possible,
e.g. (as-string 65) returns \"A\""
  (cl-typecase string-wannabe
    (string                   string-wannabe )
    (character        (string string-wannabe))
    (symbol      (symbol-name string-wannabe))
    (integer     (format "%d" string-wannabe))
    (float       (format "%g" string-wannabe))
    (t
     (error "shortcuts/as-string could not convert '%S' to string" string-wannabe)
     )))

(defalias-new 'as-string 'shortcuts/as-string)


(defmacro reverse! (seq)
  "Destructively reverse sequence SEQ and return it."
  `(setq ,seq (nreverse ,seq))
  )

(define-obsolete-function-alias 'reverse-seq! 'reverse! 28.1)


(defun msg/quiet (format-string &rest args)
"Log message but do not display it in minibuffer.
Returns the message string."
  (let1  inhibit-message  t
    (apply #'message format-string args)
    ))

(defalias 'msg/show-briefly 'minibuffer-message)

(defun msg/no-log (format-string &rest args)
  "Display message in minibuffer but do not record it in the messages log."
  (let (message-log-max)
    (apply 'message format-string args)
    ))

(defun msg/no-log/pause (seconds format-string &rest args)
  "Display message in minibuffer, then pause for SECONDS.
Does not record message in *Messages* log."
  (let (message-log-max)
    (apply 'message format-string args)
    (sit-for seconds)
    ))

(defun msg/pause (seconds format-string &rest args)
  "Display message in minibuffer, then pause for SECONDS.

Returns nil, with the idea that it can be used for warning
messages which are associated with failure of some sort.
"
  (apply 'message format-string args)
  (sit-for seconds)
  nil
  )

(defmacro imessage (format-string &rest args)
  "wrapper around `message' which only gives the message during an interactive call"
  `(if (interactive-p)
       (message ,format-string ,@args)
    ))


(defun chars-as-strings (&rest args)
  "Pass ARGS through, but with characters upgraded to strings;
non-character ARGS are passed through as is.

See also `echars-as-strings'"
  (mapcar
   (lambda (arg)
     (if (characterp arg)
         (string arg)
       arg
       ))
   args
   ))

(defun echars-as-strings (&rest args)
  "Pass ARGS through, but with characters upgraded to strings;
strings ARGS are passsed through as is.
Error on other types of ARGS.

See also `chars-as-strings'"
  (mapcar
   (lambda (arg)
     (if (characterp arg)
         (string arg)
       (unless (stringp arg) (error "Expected character or string, but got:  '%S'" arg))
       arg
       ))
   args
   ))


(defun mapcat (fun s)
  "Shortcut for (mapconcat fun s \"\"."
  (mapconcat fun s "")
  )


;; first-dwim and secnd-dwim might fit better in a package for handling pairs
(defun first-dwim (pair)
  "Return first element of PAIR;
being a dotted pair or a sequence"
  (elt pair 0)
  )

(defun secnd-dwim (pair)
  ;; spelled this way {first secnd third forth} all have length 5
  "dwim version of `second'.
Tries to give the second item of PAIR
If PAIR is a dotted pair (A . B), B is returned,
Otherwise if PAIR is a sequence its second element is returned"
  (if (consp pair)
      (let1  cell-cdr  (cdr pair)
        (if (consp cell-cdr)
            (car cell-cdr)
          cell-cdr
          ))
    (aref pair 1)
  ))

(defalias 'derZweite 'secnd-dwim)


(defmacro += (place &rest addends)
  "Like += operator of C or Perl.

PLACE can be anything acceptable to `setf',
but results are unpredictable if PLACE is a literal object.
"
  `(setf ,place (+ ,place ,@addends))
  )

(defmacro -= (place &rest addends)
  "Like -= operator of C or Perl.

PLACE can be anything acceptable to `setf',
but results are unpredictable if PLACE is a literal object.
"
  `(setf ,place (- ,place ,@addends))
  )

(defmacro *= (place value)
  "Like C or Perl *= operator"
  `(setf ,place (* ,place ,value)))

(defmacro .= (place &rest appendees)
  "Like .= operator of Perl."
  `(setf ,place (concat ,place ,@appendees))
  )

(defmacro and= (place &rest addends)
  "Analogous to += operator of C or Perl."
  `(setq ,place (and ,place ,@addends)))


(defmacro or= (place &rest values)
  "Shortcut to do nothing if PLACE is true, but otherwise assign PLACE
to the first true value in VALUES, the value is also returned.

Similar to perl default value assignment idiom."
  `(if  (not ,place)  (setf ,place (or ,@values)))
  )

(defmacro cl-delete= (item seq-place &rest cl-keys)
  "Shortcut for `setq' `cl-remove' idiom."
  (declare
   (debug (form symbol body))
   )
  `(setf ,seq-place
         (cl-delete ,item ,seq-place ,@cl-keys)
         ))

(defmacro ++ (place &optional place2)
  "Increment PLACE; and PLACE2 if given.

The current implementation
returns the new value of the last incremented place."
  (if place2
      `(progn
         (++ ,place)
         (++ ,place2)
         )
    (if (symbolp place)
        `(setq ,place  (1+ ,place))
      `(cl-callf + ,place 1)
      )))

(defmacro -- (place &optional place2)
  "Decrement PLACE; and PLACE2 if given.

The current implementation
returns the new value of the last decremented place."
  (if place2
      `(progn
         (-- ,place)
         (-- ,place2)
         )
    (if (symbolp place)
        `(setq ,place  (1- ,place))
      `(cl-callf - ,place 1)
      )))


(defmacro post++ (place)
  "Increment PLACE, but return the value before incrementing"
  `(prog1 ,place (setf ,place (1+ ,place)))
  )

(defmacro post-- (place)
  "Decrement PLACE, but return the value before decrementing"
  `(prog1 ,place (setf ,place (1- ,place)))
  )


(cl-defmacro ++roll (x limit &optional (floor 0))
  "Increment X, except roll-over to zero if FLOOR it X would exceed LIMIT.

No argument validity checking is done."
  `(setq ,x
         (if (< (1+ ,x) ,limit)
             (1+ ,x)
           ,floor)
         ))

(defmacro ++2 (place)
  "Add 2 to generalized variable PLACE"
  `(setf ,place (+ 2 ,place))
  )

(defun 2- (num)
  "Return NUM - 2"
  (- num 2))

(defun 2+ (num)
  "Return NUM + 2"
  (+ num 2))


(defmacro cdr++ (cons-cell)
  "Set CONS-CELL equal to its cdr"
  `(setq ,cons-cell (cdr ,cons-cell))
  )


(defmacro notall (&rest args)
  "True iff any arg ∈ ARGS is false.  Uses short-circuit evaluation"
  (declare (debug (&rest form)))
  `(not (and ,@args))
  )


(defun nequal (a b)
  "shortcut for (not (equal ...))"
  (not (equal a b)))


(defmacro delnil (seq)
  "Destructively Delete nil from sequence."
  `(setq ,seq (delq nil ,seq))
  )

(defun push-nonnil (elem list)
  "like push, but ignores nil elements."
  ;; to consider, define as macro so that ELEM is only evaluated once.
  (if elem (push elem list))
  )

(defun mapcar-nonnil (fun seq)
  "Wrapper around mapcar which removes non-nil elements from list to be returned."
  (delq
   nil
   (mapcar fun seq)
   ))


(defun goto-pos-unless-nil (pos)
  "Like (goto-char POS); except
if POS is nil, quietly do nothing"
  (when pos (goto-char pos))
  )

(cl-defun forward-char-safe (&optional (n 1))
  "Like (forward-char n), but when that would trigger a range error,
instead do nothing and return nil."
  (save-point-unless
   (ignore-error args-out-of-range
     (forward-char n)
     t
     )))


(cl-defun forward-line-create (&optional (n 1))
  "Like `forward-line', but add newlines to buffer if necessary.
Currently only positive values of N are supported.

Does *not* override read-only status of buffer.
"
  (cl-assert (<= 0 n) "Currently negative values of N are not supported")
  (dotimes (_ (forward-line n))
    (insert "\n")
    ))


(defun insert-safe (&rest args)
  "Like `insert', but quietly skips nil ARGS.
Returns true unless all args are nil."
  (let1  non-nils  (delnil args)
    (when non-nils
      (apply #'insert non-nils)
      (car args); Always true.
    )))

(defun insertt (&rest args)
  "Like `insert' but returns t."
  (apply #'insert args)
  t
  )

(defun insertf (format-string &rest args)
  "Insert args into current buffer;
formated by `format' according to FORMAT-STRING."
  (insert (apply #'format format-string args))
  )

(defun insert-clf (format-string &rest args)
    "Insert args into current buffer;
formated by `cl-format' according to FORMAT-STRING."
    (apply #'cl-format (current-buffer) format-string args)
  )


(defun insert-1num (arg)
  "Insert ARG; numerically, when appropriate."
  (insert
   (format
    (cl-typecase arg
      (integer "%d")
      (number  "%g")
      (t       "%s")
      )
    arg
    )))


;; Analog of `insert' which can handle numerical type arguments.
;; Maybe the interactive version of this should have a different name.
(defun insert-num (&rest args)
  "Insert ARGS; numerically, when appropriate.
When called interactively prompt for an elisp expression."
  (interactive
   (list
    (eval-minibuffer "numerical expression: ")
    ))
  (while args
    (insert-1num (pop args))
    ))


(defun ln-insert (&rest args)
  "Insert a newline, then ARGS"
  ;; used to be called insertn
  (apply 'insert "\n" args)
  )

(defun insert-ln (&rest args)
  "Insert ARGS, then a newline"
  ;; used to be called insertn
  (apply 'insert args)
  (insert "\n")
  )

;; Previously named text-insert-at PH20241127
(defun insert-at-pos (pos &rest args)
  "Insert ARGS at position POS; then restore point.
Returns position in buffer after the insertion."
  (save-excursion
    (goto-char pos)
    (apply #'insert args)
    (point)
    )
  )

;; Commented out PH20241127
;; If reinstated it should probably be renamed.
;; perhaps to:  insert-at-pos/before-markers
;; (defun text-insert-before (pos &rest args)
;;   "Insert args before markers at position POS; then restore point."
;;   (save-excursion
;;     (goto-char pos)
;;     (apply #'insert-before-markers args)
;;     )
;;   )


(defun insert-as-text (thing)
  "Try to insert THING as type in current buffer,
where THING maybe any one of a variety of types."
  ;; to consider.  Maybe add optional buffer argument.
  (cl-typecase thing
    (buffer
     (insertf "%S" thing)
     )
    (string
     (insertf "%s" thing)
     )
    (symbol
     (insert "'" (symbol-name thing))
     )
    (vector
     (princ thing (current-buffer))
     )
    (t
     (insert thing)
     )))


(defun text-inserter (&rest args)
  "Return function which inserts ARGS at point"
  `(lambda () (insert ,@args)
  ))

(defun text-deleter (len)
  "Return function which deletes up to LEN characters at point,
and returns the number of deleted chars"
  `(lambda ()
     (let1  actual-len  (max ,len (- (point-max) (point)))
       (delete-char actual-len)
       )))


(defalias 'goto-bol 'beginning-of-line)
(defalias 'goto-eol       'end-of-line)


(defun goto-bob ()
  "Move point to Beginning Of Buffer."
  (goto-char (point-min))
)

(defun goto-eob ()
  "Move point to End Of Buffer."
  (goto-char (point-max))
  )


(defun gotoline (line-num)
  "Move point to beg of line LINE-NUM.
Numbered from 1 starting at point-min.

If LINE-NUM is in range, return 0

If line-num is out of range,
silently go to the final line;

and return the number of lines left to move.

Non-interactive, alternative to `goto-line'.
See also `went-to-line?'
"
  (goto-bob)
  (forward-line (1- line-num))
  )

(defun went-to-line? (line-num)
  "If LINE-NUM in range, go to it
and return a true value.
Otherwise do nothing and return nil.

LINE-NUM Numbered from 1 starting at `point-min'."
  (let1  point-ori   (point)
    (goto-bob)
    (if (= 0 (forward-line (1- line-num)))
        t
      (goto-char point-ori)
      nil
      )))

(defun cur-line (&optional drop-props?)
  "Return point line as a string.

See also: (thing-at-point 'line)
"
  (buffer-substring* (bol-pos) (eol-pos) drop-props?)
  )

(defun cur-line-num (&optional absolute?)
  "Return line number of current line.
ABSOLUTE? is passed to `line-number-at-pos'"
  ;; For discussion of using `format-mode-line', perhaps faster than `line-number-at-pos' see:
  ;; https://emacs.stackexchange.com/questions/3821/a-faster-method-to-obtain-line-number-at-pos-in-large-buffers
  (line-number-at-pos (point) absolute?)
  )

(defun line-number-at-eob (&optional absolute?)
  "Return line number of the end of buffer.
ABSOLUTE? is passed to `line-number-at-pos'"
  (line-number-at-pos (point-max) absolute?)
  )

(cl-defun move-to-prev-line-column (&optional (n 1) (col (current-column)))
  "Move point upward N lines, at column COL.
Returns the number of lines left to move (see `forward-line')."
  (prog1
      (forward-line (- n))
    (move-to-column col)
    ))

(defun insert-rectangle/stay-top (rect)
  "Like `insert-rectangle'; but leave point on the top line"
  (insert-rectangle rect)
  (move-to-prev-line-column (1- (length rect)))
  )

;; bounds returned here are a cons cell holding range (beg . end)
;; region-bounds, in contrast, would return ((beg . end)) so that it
;; can support multiple ranges (as in discontiguous regions of a rectangle)
;; I think bounds could be defined as an abstract type which accepts either
;; So a single range can be represented as (beg . end) or ((beg . end))
;; While multiple ranges (when applicable) would be ((beg1 . end1) (beg2 . end2)...)

(defun cur-line-bounds ()
  "Return bounds of point line"
  (cons (bol-pos) (eol-pos))
  )

(defun abs-line-bounds (line-num)
  "Return bounds of line number LINE-NUM;
or nil if LINE-NUM is out of bounds"
  (cl-check-type line-num (integer 1 *))
  (save-excursion
    (when (went-to-line? line-num)
      (cons (bol-pos) (eol-pos))
      )))

(defun rel-line-bounds (line-num)
  "Return bounds of relative line number LINE-NUM;
or nil if LINE-NUM is out of bounds

LINE-NUM is the number of lines relative to
the line point is on.

Negative value means before the point line."
  (cl-check-type line-num integer)
  (save-excursion
    (if (= 0 (forward-line line-num))
        (cons (bol-pos) (eol-pos))
      )))


(cl-defun goto-char-relative (displacement &key (anchor (point)))
  "Like (goto-char DISPLACEMENT), but relative to ANCHOR position.
Like `goto-char', if the new position would be out of range of the
buffer, just quietly go as far as possible.

Returns the distance moved.
For example:
  ;; assuming point-min is 1
  (goto-char-relative -7 9);  --> returns 7
  (goto-char-relative -7 3);  --> returns 2
"
  (goto-char (+ anchor displacement))
  (abs (- anchor (point)))
  )


(defun last1 (L &optional n)
  "Return the final element of list L;
or if number N given, the Nth to the last element.

Extension of last1 described in P. Graham's On Lisp."
  (car (last L n))
  )

(defun nth-circ (i lista)
  "Like `nth' except negative index I counts from end of the SEQ.
Quietly returns nil if index I is out of range."
  (cl-assert (listp lista) t)
  (if (<= 0 i)
      (nth i lista)
    (let* (
           (len  (length lista))
           (idx  (+ len i))
           )
      (when (<= 0 idx)
        (nth idx lista)
        ))))

(defmacro push2 (elt1 elt2 place)
  "Add elements ELT1 and ELT2 to the head of place
Equivalent to (setf PLACE (cons (cons ELT1 ELT2)) PLACE))"
;; This implementation wastefully evaluates PLACE twice
;; Todo, mimic the way push works so PLACE can be evaluated only once.
  `(progn
     (push ,elt2 ,place)
     (push ,elt1 ,place)))


(defun aref-quiet (array idx)
  "Like `(aref ARRAY IDX), except quietly return nil when index IDX is out of range"
  (when (< idx (length array))
    (aref array idx)
    ))

(defun cadr-safe (L)
  "Like `cadr', but quietly return nil when L is not a list.
Named after `car-safe' and `cdr-safe'"
  ;; Note `cadr' and `caddr' are already "safe" when passed lists which are too short.
  ;; but they signal an error when L is not a list.
  (let1  cdr-L  (cdr-safe L)
    (when (consp cdr-L)
      (car-safe cdr-L)
      )))

(defun aref-safe (array n)
    "Nth item of ARRAY
or nil if N is out of range.

Note nil is return in 2 cases:
 * the Nth value of ARRAY is nil
 * N is out of range for ARRAY

Named after `car-safe' and `cdr-safe'"
    (and
     (length> array n)
     (aref array n)
     ))

(defun elt-safe (seq n)
    "Nth item of sequence SEQ,
or nil if N is nil or out of range.
Named after `car-safe' and `cdr-safe'"
    (and n
         (if (listp seq)
             (elt seq n)
           (and (length> seq n)
                (aref seq n)
                ))))


(defun assoc= (key alist)
  "Like assoc, but test using ="
  (cl-assoc key alist :test #'=)
  )

(defun assoc-string* (key alist &optional case-fold)
  "Like `assoc-string', but KEY can be a character;
in which case it treated like a one character string."
  (assoc-string
   (if (characterp key) (string key) key)
   alist
   case-fold
   ))

(defmacro cdrassoc= (key alist)
  "Shortcut for (cdr (assoc= key ALIST))"
  `(cdr (assoc= ,key ,alist)))

(defmacro cdrassq (key alist)
  "Shortcut for (cdr (assoc key ALIST))"
  `(cdr (assq ,key ,alist)))

(defmacro cdrassoc (key alist)
  "Shortcut for (cdr (assoc key ALIST))"
  `(cdr (assoc ,key ,alist)))

(defmacro cdrassoc-string (key alist)
  "Shortcut for (cdr (assoc-string key ALIST))"
  `(cdr (assoc-string ,key ,alist))
  )

(defun cdrassoc-string* (key alist &optional case-fold)
  "Shortcut for (cdr (assoc-string* KEY ALIST CASE-FOLD))"
  (cdr (assoc-string* key alist case-fold))
  )

(defmacro cadrassoc (key alist)
  "Shortcut for (cadr (assoc key ALIST))"
  `(cadr (assoc ,key ,alist)))

(defun carrassoc (key alist)
  "Shortcut for (car (rassoc key ALIST))"
  (car (rassoc key alist))
  )

(defun symbol-names (symbols)
  "Return a list of the names of symbols in SYMBOLS."
  (mapcar #'symbol-name symbols)
  )



(defalias 'qq 's-lex-format
  "Give s-lex-format by Nic Ferrier a shorter name (from Perl).")


(defun argmax (&rest args)
  "Return index of largest arg.  Ties go to first occurrence"
    (let ((maxVal (pop args))
          (maxI   0)
          (i      0)
          val
          )
      (while args
        (++ i)
        (when (> (setq val (pop args)) maxVal)
          (setq maxVal val
                maxI i)))
      maxI))


(defun minmax (num1 &rest more-nums)
  "Return 2-element list holding the minimum and maximum value of NUMS.
NUMS may be markers but the returned values are plain numbers.

\(fn nums...)"
  (list
   (apply #'min num1 more-nums)
   (apply #'max num1 more-nums)
   ))


(defmacro bebest! (best-selector first &rest others)
  "Set symbol FIRST to the best value of all the arguments;
including FIRST itself (unless FIRST is nil)

Error if FIRST is unbound.

best-selector should be a function or macro that select
one \"best\" value from a list of non-nil values; e.g. min, max"
  ;; I was once tempted to allow FIRST to be an unbound symbol
  ;; since setq allows that, but realized that approach
  ;; would not be robust to mistyped variable names.
  (declare
   (indent 2)
   (debug (symbol symbol body))
   )
  `(setq ,first
         (if ,first
            (,best-selector ,first ,@others)
           (,best-selector ,@others)
           )))


(defmacro bemin! (first &rest others)
  "Set symbol FIRST to equal the minimum value of all the arguments;
including FIRST itself (unless FIRST is nil or unbound).

A non-nil, non-numerical value of FIRST signals an error."
  (declare
   (indent 1)
   (debug (symbol body))
   )
  `(bebest! min ,first ,@others)
  )

(defmacro bemax! (first &rest others)
  "Set symbol FIRST to equal the maximum value of all the arguments;
including FIRST itself (unless FIRST is nil).

A non-nil, non-numerical value of FIRST signals an error."
  (declare
   (indent 1)
   (debug (symbol body))
   )
  `(bebest! max ,first ,@others)
  )


(defun substring* (sg &optional from to sans-props?)
  "Dispatch to substring(-no-properties) according to SANS-PROPS?,
FROM defaults to zero
TO   defaults to the length of the string SG

Case type on SANS-PROPS?

  nil               Return substring including text properties
  number            Signals error
  true value*       Return substring without text properties

true value* means any non-numerical true value.
This requirement allows incorrect argument order to be detected.
"
  (cl-check-type sg string)
  (if from  (cl-check-type from integer)  (setq from 0))
  (if to    (cl-check-type from integer)  (setq to (length sg)))
  (declare (pure t) (side-effect-free t))
  (if sans-props?
      (if (numberp sans-props?)
          (error "substring* numerical SANS-PROPS? value %g not allowed." sans-props?)
        (substring-no-properties sg from to)
        )
    (substring  sg from to)
    ))



(defun shortcuts/sans-textprops (arg)
  "Return text property free copy of ARG;
deep copy except strings have no text properties."
  ;; useful when using `eval-expression' to look at return values in the echo area
  (cond
   ((stringp arg)
    (substring-no-properties arg)
    )
   ((listp arg)
    (cl-map 'list   #'shortcuts/sans-textprops arg)
    )
   ((vectorp arg)
    (cl-map 'vector #'shortcuts/sans-textprops arg)
    )
   (t arg)
   ))

(defalias-new 'sans-textprops 'shortcuts/sans-textprops)

(defun clear-text-properties (&rest strings)
  "Remove all text properties from STRINGS"
  ;; From before I made sans-textprops, not sure how useful this function is now PH20241204.
  (dolist (S strings)
    (set-text-properties 0 (length S) nil S)))



;; ────────────────────  Regex functions  ────────────────────
;; To consider, maybe think of a good prefix for these and fork them out of shortcuts.
(defun re-search-forward/goto-beg (regexp &optional bound noerror count)
  "Like `re-search-forward', but places point at the start of the match.

Caveat: successive calls will return the same match.

See also `re-search-forward/after-point'.
"
  (interactive "sRE search: ")
  (when (re-search-forward regexp bound noerror count)
    (forward-char (- (length (match-string 0))))
    (point); return point upon successful search.
    ))



;; To consider, maybe think of a good prefix for these and fork them out of shortcuts.
(defun re-search-forward/after-point (regexp &optional bound noerror count)
  "Like (re-search-forward BOUND t COUNT); except

 - puts `point' at the start of the match.

 - Searches starting from the position just after `point';
   otherwise successive calls would always return the same match.

 - Returns true when a match if found, otherwise nil.

When no match is found, just return nil.
"
  (interactive "sRE search: ")
  (unless (eobp)
    (goto-pos-unless-nil
     (save-excursion
       (forward-char)
       (when (re-search-forward regexp bound noerror count)
         (match-beg 0)
         )))))



;; ──────────  Functions simply accessing the match data  ──────────
(defun match-string* (num text props?)
  "Like `match-string' with or without text properties, depending on PROPS?
Use nil for TEXT if last search was made against the current buffer"
  (funcall
   (if props? #'match-string #'match-string-no-props)
   num
   text
   ))


(cl-defun match-len (&optional (subexp-num 0))
  "Length of match-string SUBEXP-NUM.
Unlike `match-string', a string argument is never needed."
  (declare (side-effect-free t))
  (- (match-end subexp-num) (match-beg subexp-num))
  )

(defun match-as-num (num &optional string)
  "Return (match-string NUM STRING) converted to a number with `string-to-number'"
  (string-to-number
   (substring-no-properties string
                            (match-beginning num)
			    (match-end num)
                            )))

;;  Want to count matches?
;;  Try s-count-matches[-all] in s.el



;; ──────────  Functions doing regex matching and then accessing the match data  ──────────
(cl-defun regex-matches (regex text &rest group-idxs)
  "`regex-matches-from-pos' starting at beginning of TEXT"
  (apply #'regex-matches-from-pos regex text 0 group-idxs)
  )

(cl-defun regex-matches-from-pos (regex text start &rest group-idxs)
  "If string TEXT (from START) matches REGEX, return list of group matches;
GROUP-IDXS should be integers ≧0 representing match group numbers
to forward to `match-data',

The same group idx can appear more than once in GROUP-IDXS.
Error reported if any GROUP-IDXS are out of range.

Quietly return nil when TEXT (from START) does not match REGEX.

Similar to `s-match', but also selects which groups to return.
"
  (if-let1 matches (s-match regex text start)
      (let (
            (limit (length matches))
            acc
            )
        (dolist (group-idx group-idxs)
          (cl-check-type group-idx (integer 0 *))
          (if (< group-idx limit)
              (push (nth group-idx matches) acc)
            (error "group-idx %d ≧ limit %d" group-idx limit)
            ))
        (reverse acc)
        )))



;; Change notice: PH20241204
;; regex-matches runs `string-match' internally, so the code
;; preceding regex-matches may not make it obvious that the
;; "matches" are regex matches.

;; So I decided simply `matches' was not informative enough as a name.
;; Similar for `matches-from' renamed to `regex-matches-from-pos'
(define-obsolete-function-alias 'matches 'regex-matches "PH20241204")
(define-obsolete-function-alias 'matches-from 'regex-matches-from-pos "PH20241204")



(cl-defun bufpos-apply-unifun (pos fun fun-arg &key nil-pos-ok out-of-range-ok)
  "If buffer position POS is in range;
go there and apply unary function FUN with FUN-ARG.

The function call is wrapped in `save-excursion'

Usually signals an error when BUFPOS is out or range or not a number-or-marker.
Except:
  when nil-pos-ok, quietly return nil, when given a nil BUFPOS
  when out-of-range-pos-ok, quietly return nil when BUFPOS is out of range."
  (ifnot pos
      (if nil-pos-ok
          nil
        (error "bufpos-apply-unifun expected a buffer position but got nil.")
        )
    (cl-check-type pos integer-or-marker)
    (ifnot (≦ (point-min) pos (point-max))
        (if out-of-range-ok
            nil
          (signal 'args-out-of-range (list 'buffer-pos pos (list (point-min) (point-max))))
          )
      (save-excursion
        (goto-char pos)
        (funcall fun fun-arg)
      ))))


(defun delete-char-befor-pos (pos &optional n)
  ;; "befor" to match length of "after"
  "Delete character before POS; or with N, delete N characters."
  (delete-region (- pos (or n 1)) pos)
  )

(defun delete-char-after-pos (pos &optional n)
  "Delete character after POS; or with N, delete N characters."
  (delete-region pos (+ pos (or n 1)))
  )


(cl-defun bufpos-looking-at (bufpos regex &key nil-pos-ok out-of-range-ok)
  "Is BUFPOS an in range buffer position `looking-at?' REGEX?
if so return the match.

when NILOK? a nil BUFPOS is quietly returned instead of signaling an error.

Restores point.
Match groups can extracted from the match data."
  (bufpos-apply-unifun bufpos #'looking-at regex :nil-pos-ok nil-pos-ok :out-of-range-ok out-of-range-ok)
  )


(cl-defun bufpos-looking-at? (bufpos regex &key nil-pos-ok out-of-range-ok)
  "Is BUFPOS an in range buffer position `looking-at?' REGEX?
if so return the match.

when NILOK? a nil BUFPOS is quietly returned.

Match Data: restored."
  (bufpos-apply-unifun bufpos #'looking-at? regex :nil-pos-ok nil-pos-ok :out-of-range-ok out-of-range-ok)
  )


(cl-defun bufpos-looking-bk? (bufpos regex &key nil-pos-ok out-of-range-ok)
  "Is BUFPOS an in range buffer position `looking-at?' REGEX?
if so return the match.

when NILOK? a nil BUFPOS is quietly returned.

Match Data: restored."
  (bufpos-apply-unifun bufpos #'looking-bk? regex :nil-pos-ok nil-pos-ok :out-of-range-ok out-of-range-ok)
  )



(cl-defun string-prefix? (text-string maybe-prefix &optional ignore-case)
  "If TEXT-STRING starts with MAYBE-PREFIX, return MAYBE-PREFIX.

Otherwise return nil.

Never alters the match data.
See also `string-prefix-p';
But note difference in return value, argument order, and use of named options."
  (let1  target-len  (length maybe-prefix)
    (if (≦ target-len (length text-string))
        (and
         (t?
          (compare-strings text-string  0 target-len
                           maybe-prefix 0 target-len
                           ignore-case
                           ))
         maybe-prefix
         ))))


(cl-defun string-suffix? (text-string maybe-suffix &optional ignore-case)
  "If TEXT-STRING ends with MAYBE-SUFFIX, return MAYBE-SUFFIX.

Otherwise return nil.

See also `string-suffix-p';
But note difference in return value, argument order, and use of named options."
  (let1  start-pos  (- (length text-string) (length maybe-suffix))
    (if (≦ 0 start-pos)
         (and
          (t?
           (compare-strings text-string  start-pos nil
                            maybe-suffix nil       nil
                            ignore-case
                            ))
          maybe-suffix
          ))))



(cl-defun buffer-lines ()
  "Return lines of current buffer as a list of strings."
  ;; Common special case of region lines or rectangle lines.
  ;; In the future, probably will want to add ability to skip comment lines,
  ;; or strip text properties etc.
  (let (ret-list)
    (save-excursion
      (goto-bob)
      (while (not (eobp))
        (push (buffer-substring (point) (line-end-position)) ret-list)
        (forward-line 1)
        ))
    ret-list
    ))


(defvar temp-buffer/less-ephemeral
  (get-buffer-create "*temp-buffer/less-ephemeral*")
  "Temporary buffer for general use, including regression testing.
Similar to using `with-temp-buffer'; but less ephemeral because
it is not automatically cleaned up, like the temp buffers used
in `with-temp-buffer' expression."
  )

(defmacro try-in-temp-buffer (contents &rest body)
  "Do BODY in buffer `temp-buffer/less-ephemeral' initialized with CONTENTS.
Before evaluating BODY `point' is placed at the beginning of the buffer.
Returns buffer as a string.

Useful in ert tests for functions acting on buffers."
  ;; Note: we use a buffer with known name *try-in-temp-buffer*
  ;; for easy of debugging (as compared to using `with-temp-buffer')
  ;; to consider:
  ;; support non-insertable types for CONTENTS; buffers, for example.
  ;; support explicit return of a value,
  ;; with the buffer string then being a fallback default
  (declare (indent 1))
  `(save-current-buffer
     (with-current-buffer temp-buffer/less-ephemeral
       (erase-buffer)
       (insert ,contents)
       (goto-char (point-min))
       (progn ,@body)
       (buffer-string)
       )))

(defmacro with-temp-buffer/string (&rest body)
  "Run body in temp buffer and return `buffer-string'.

Buffer `temp-buffer/less-ephemeral' is used.
"
  (declare
   (debug (body))
   )
  `(progn
     (with-current-buffer temp-buffer/less-ephemeral
       (erase-buffer)
       ,@body
       (buffer-string)
       )))

(defmacro with-temp-buffer/string= (string &rest body)
  "Run body in temp buffer and test if its contents equal STRING.
Useful in ert tests.

Buffer `temp-buffer/less-ephemeral' is used.
"
  (declare
   (indent 1)
   (debug (form body))
   )
  `(string=
    ,string
    (with-temp-buffer/string ,@body)
    ))


;;; Sort related functions
;;
(defun car-string-less-than-car (a b)
  "Like `car-less-than-car' but compares with `string<'"
  ;; to consider: profiling speed and possibly using define-inline
  (string< (car a) (car b))
  )

(defalias 'car-string<-car 'car-string-less-than-car)

(defun sort-by-car (seq pred)
  "Sort sequence SEQ using car's of each element as a key."
  ;; to consider: profile and possibly reimplement as macro.
  (sort
   seq
   (lambda (a b)
     (funcall pred (car a) (car b))
     )
   ))


;;; Common Lisp related functions
;;
(defmacro cl-assertf (form msg-format &rest args)
  "If FORM is nil, do (error MSG-FORMAT ARGS).

(cl-assertf (< 0 x) \"number %d too small\" x)

Is like
(cl-assert (< 0 x) nil (format \"number %d too small\" x))
"
  (declare (debug (form &rest form)))
  (and (or (not (macroexp-compiling-p))
	   (< cl--optimize-speed 3) (= cl--optimize-safety 3))
       `(or ,form (error ,msg-format ,@args))
       ))


(defun cl-complement (pred)
  "Return a \"logically negated\" version a function PRED.
The returned value is a closure which itself
always returns t or nil."
  (lambda (&rest args)
    (not (apply pred args))
    ))

(defun maybe-cl-complement (pred complement?)
  "If COMPLEMENT? return `cl-complement' of PRED,
otherwise pass PRED through as is"
  ;; Shortens code when PRED is a long function name or lambda expression
  (if complement? (cl-complement pred) pred)
  )


(defun macroexpand-2 (form &optional environment)
  "Perform (at most) two steps of macroexpansion."
  (macroexpand-1
   (macroexpand-1
    form environment
    )))


;; Register related shortcuts.
;; To consider: Add more of these all the way down to regrz (or up to regA)
;; Could be down manually or via a macro.
(defun regra ()  "Value of register a"  (get-register ?a))
(defun regrb ()  "Value of register b"  (get-register ?b))
(defun regrc ()  "Value of register c"  (get-register ?c))
(defun regrd ()  "Value of register d"  (get-register ?d))

(defun regra: (val)  "Set value of register a"  (set-register ?a val))
(defun regrb: (val)  "Set value of register b"  (set-register ?b val))
(defun regrc: (val)  "Set value of register c"  (set-register ?c val))
(defun regrd: (val)  "Set value of register d"  (set-register ?d val))



;;  Function on sequence lengths.
;;  useful for reality checking argument validity
;;  Not sure if they really belong in shortcuts.el

(cl-defun seqs-length-cmp (cmp-fun seq1 more-seqs-LIST)
  "Test if sequence lengths all fulfill (CMP-FUN len1 len2);
if so, return length of the SEQ1;
otherwise nil.

The sequences are SEQ1 and the elements of MORE-SEQS-LIST
which should be a list of sequences
"
  (cl-check-type seq1 sequence)
  (let1  len1  (length seq1)
    (dolist (seq more-seqs-LIST)
      (cl-check-type seq sequence)
      (unless
          (funcall cmp-fun len1 (length seq))
        (cl-return-from seqs-length-cmp nil)
        ))
    len1
    ))

(defun seqs-length=? (seq1 &rest more-seqs)
  "If all seqs have equal length return that length, otherwise return nil."
  ;; Could be specialized to make use of C builtin function `length='
  (seqs-length-cmp #'= seq1 more-seqs)
  )

(defun seqs-length<? (seq1 &rest more-seqs)
  "Iff all seqs have lengths in strictly ascending order return true.
When true, the length of SEQ1 is returned."
  ;; Could be specialized to make use of C builtin function `length<'
  (seqs-length-cmp #'< seq1 more-seqs)
  )

(defalias 'seqs-len= 'seqs-length=?)
(defalias 'seqs-len< 'seqs-length<?)




(provide 'shortcuts)

;;; shortcuts.el ends here
