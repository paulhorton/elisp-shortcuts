;;; shortcuts-math-symbols-basic.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20240609
;; Updated: 20240609
;; Version: 0.0
;; Keywords:

;;; Commentary:

;; Provide some convenience aliases for math Unicode symbols
;; which are universally recognized and essentially self-explanatory
;; for most readers (including those not particularly mathematically inclined)


;;; Change Log:

;;; Code:



(defalias '≦ '<= "Standard math symbol for less than or equal")

(defalias '≧ '>= "Standard math symbol for greater than or equal")

(defalias '≠ '/= "Standard math symbol for not equal.")
(put '≠ 'byte-compile-negated-op '=)



(with-eval-after-load 'elisp-mode
  (mapc
   (lambda (char) (modify-syntax-entry char "_" emacs-lisp-mode-syntax-table))
   "≦≧≠"
   ))



(provide 'shortcuts-math-symbols-basic)

;;; shortcuts-math-symbols-basic.el ends here
