;;; shortcuts-tests.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; License: GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231004
;; Updated: 20241009
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(ert-deftest shortcuts/with-current-buffer* ()
    (should
     (=
      5
      (with-temp-buffer
        (insert "1234")
        (with-current-buffer* nil
          (point)
          )))))


(ert-deftest shortcuts/and1 ()
  (should
   (eq
    'apple
    (and1 'apple 'pear)
    ))
  (should
   (eq
    nil
    (and1 nil 'pear)
    ))
  (should
   (eq
    nil
    (and1 'apple nil)
    ))
  (should
   (= 6
      (let1 x 5
        (and1
            (cl-incf x);  should be evaluated first
          (< 5 x)
          ))
      ))
  )


(ert-deftest matches ()
  (should
   (equal
    '("23" "cat" "23")
    (regex-matches
     "\\([0-9]+\\)\\([a-z]+\\)"
     "23cat"
     1 2 1
     )
    )))


(ert-deftest += ()
  (should
   (= 7
      (let1 x 3
        (+= x 4)
        x)
      ))
  (should;; try generalized variable
   (equal
    '(13 5)
    (let1 L (list 6 5); copy list, as behavior of += on literal objects is unpredictable
      (+= (car L) 7)
      L)
    )))


(ert-deftest post++ ()
  (should
   (equal
    [7 8]
    (let1 x 7
      (vector (post++ x) x)
      ))
   ))



(ert-deftest seqs-length=? ()
  (should
   (=
    3
    (seqs-length=? [a b c]
                  '(a b c)
                  )
    ))
   (should
    (not
     (seqs-length=? [a b c]
                   '(a b c d)
                   )
     ))
   )


(ert-deftest shortcuts/++/symbol ()
  (let ((x 4) (y 6) (z 9))
    (should
     (= 5
        (progn
          (++ x)
          x
          )
        ))
    (should
     (equal
      '(7 10)
      (progn
        (++ y z)
        (list y z)
        )))
    ))

(ert-deftest shortcuts/++/gv ()
  (should
   (equal '(5 7)
      (let1  L  (copy-sequence '(4 7))
        (++ (car L))
        L
        )))
  (should
   (equal
    '((4 7) (10 20 30 40))
    (let1  L  (copy-tree '((3 7) (10 20 29 40)))
      (++
       (caar L)
       (nth 2 (cadr L))
       )
      L
      )
    ))
   )

(ert-deftest shortcuts/--/symbol ()
  (should
   (= 3
      (let1 x 4
        (-- x)
        x
        )
      ))
  (should
     (equal
      '(5 8)
      (let ((y 6) (z 9))
        (-- y z)
        (list y z)
        )
    )))


(ert-deftest shortcuts/cl-delete= ()
  (should
   (equal '(dog)
          (let1 L '(cat dog cat)
            (cl-delete= 'cat L)
            L;  after plain cl-delete, L would be '(cat dog)
            )
          )))


(ert-deftest shortcuts/cl-complement ()
  (should
   (not
    (funcall  (cl-complement #'zerop)  0)
    ))
  (should
   (funcall  (cl-complement #'zerop)  5)
   ))

(ert-deftest shortcuts/maybe-cl-complement ()
  (should
   (not
    (funcall  (maybe-cl-complement #'cl-oddp t)  5)
    ))
  (should
   (funcall  (maybe-cl-complement #'cl-oddp nil)  5)
   ))



(ert-deftest shortcuts/chars-as-strings ()
    "test for `chars-as-strings'"
    (should
     (equal '("cat" "a" 3.15 "dog")
            (chars-as-strings "cat" ?a 3.15 "dog")
            )
     ))

(ert-deftest shortcuts/mapcar-nonnil ()
    "test for `mapcar-nonnil'"
    (should
     (equal
      '(3 bbb)
      (mapcar-nonnil
       #'identity
       '(nil 3 nil nil bbb nil)
       ))
     ))


(ert-deftest shortcuts/string-prefix? ()
  "test for `string-prefix?'"
  (should
   (string=
    "mis"
    (string-prefix? "Missouri" "mis" :ignore-case)
    ))
  (should
   (string=
    ""
    (string-prefix? "cat" "")
    ))
  (should
    (null
     (string-prefix? "cat" "cag")
     ))
  )


(ert-deftest shortcuts/ifnot ()
  "test `ifnot'"
  (should
   (eq 'cat
      (ifnot (< 5 3)
          'cat
        'dog
        'pig
        )))
  (should
   (eq 'pig
      (ifnot (< 3 5)
          'cat
        'dog
        'pig
        ))))


(ert-deftest shortcuts/: ()
  "test `:'"
  (should
   (string=
    "greater"
    (: (< 2 3)
       "greater"
       "less"
       )
    ))
  (should
   (string=
    "less"
    (: (< 3 2)
       "greater"
       "less"
       )
    ))
  )



(ert-deftest shortcuts/pop-if ()
  "Test `pop-if'"
  (should
   (equal
    '(2 (a b))
    (let ((A  '(t t a b))
          (count  0)
          )
      (while (pop-if #'t? A)
        (++ count)
        )
      (list count A)
      )))
  (should
   (equal
    '(3 nil (4 5))
    (let ((A  '(3 4 5))
          A1 A2
          )
      (setq A1 (pop-if #'cl-oddp A))
      (setq A2 (pop-if #'cl-oddp A))
      (list A1 A2 A)
      )
    )))



(ert-deftest shortcuts/if-call ()
  "Test `if-call'"
  (should
   (string=
    "CAT"
    (if-call t   'upcase 'downcase "cAt")
     ))
  (should
   (string=
    "cat"
    (if-call nil 'upcase 'downcase "cAt")
    ))
  (should
   (=
    12
    (if-call (< 3 5) '+ '* '3 4 5)
    ))
  (should
   (=
    60
    (if-call (> 3 5) '+ '* '3 4 5)
    ))
  )


(ert-deftest shortcuts/empty? ()
  "Test `empty?'"
  ;; Non-sequence arguments should give an error.
  (should-error  (empty? 3))
  (should-error  (empty? '(a . b)))
  ;; Empty sequences
  (should  (empty? '()))
  (should  (empty? ""))
  (should  (empty? []))
  ;; Non-empty sequences
  (should  (not (empty? '(a))))
  (should  (not (empty? "a")))
  (should  (not (empty? [a])))
  )


(ert-deftest shortcuts/delnil ()
    (should
     (equal
      '("str" sym 39)
      (let1  lista  '(nil nil "str" nil sym nil 39)
        (delnil lista)
      )))
    )


(ert-deftest shortcuts/neq? ()
  (should-error
   (neq? 'a)
   :type 'wrong-number-of-arguments
    )
  (should
   (null
    (neq? 'a 'a)
    ))
  (should
   (null
    (neq? 23 23 23)
    ))
  (should
   (eq
    'b
    (neq? 'a 'b)
    ))
  (should
   (eq
    'b
    (neq? 'a 'a 'b)
    ))
  (should
   (eq
    t
    (neq? 'a nil 'a)
    ))
  (should
   (eq
    t
    (neq? 'a 'a t)
    ))
  )


(ert-deftest shortcuts/unequal? ()
  (should
   (eq
    'dog
    (unequal? 'cat 'dog)
    ))
  (should
   (eq 'dog
    (unequal? nil 'dog)
    )
    )
  (should
   (eq t
    (unequal? 'dog nil)
    )
    )
  (should
   (null
    (unequal? 'cat 'cat)
    )
    )
  )


(ert-deftest shortcuts/minmax ()
  (should
   (equal
    '(3 3)
    (minmax 3)
    )
   )
  (should
   (equal
    '(4 9)
    (minmax 9 7 6 4 7)
    ))
  )

(ert-deftest shortcuts/bemax! ()
  (should
   (= 9
      (let1 x 7
        (bemax! x 3 9)
        x
        )
      ))
  (should
   (= 17
      (let1 x 17
        (bemax! x 9)
        x
        )
      ))
  (should
   (= 8
      (let (x); x is nil
        (bemax! x 8)
        x
        )
      ))
  )


(ert-deftest shortcuts/string/elisp-reads-as-number? ()
  (should
   (= 23
      (string/elisp-reads-as-number? "23")
      )
   )
  (should
   (eq nil
       (string/elisp-reads-as-number? " 23")
       )
   )
  (should
   (eq 23
       (string/elisp-reads-as-number? " 23" t)
       )
   )
  (should
   (eq nil
       (string/elisp-reads-as-number? "0.1h")
       )
   ))


;;; shortcuts-tests.el ends here
