;;; qw-tests.el --- ert tests for qw  -*- lexical-binding: t -*-

;; Copyright (C) 2024, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20240613
;; Updated: 20240613
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:

(require 'qw)


(ert-deftest qw ()
  (should
   (equal '("fred" "ted")
          (qw fred ted))
   )
  (should
   (equal
    '("a" "b" ("c" "d") "z")
    (qw a b (c d) z))
   ))


;;; qw-tests.el ends here
