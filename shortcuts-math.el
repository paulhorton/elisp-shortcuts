;;; math-shortcuts.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2021, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20211027
;; Updated: 20211027
;; Version: 0.0
;; Keywords:

;;; Commentary:

;;; Change Log:

;;; Code:


(require 'let1)



;; ──────────  Filter friendly predicates  ──────────
(defun int-even? (num)
  "If putative NUM is an even integer return it, otherwise nil."
  (and (integerp num) (cl-evenp num) num)
  )

(defun int-odd? (num)
  "If putative NUM is an odd integer return it, otherwise nil."
  (and (integerp num) (cl-oddp num) num)
  )


(defun lg (x)
  "log₂(𝘹)
In the special case that x is an integer power of two and 1 ≦ x ≦ 60;
an integer value is returned."
  (or (cdr (assoc x
                  '((1 . 0) (2 . 1) (4 . 2) (8 . 3) (16 . 4) (32 . 5) (64 . 6) (128 . 7) (256 . 8) (512 . 9) (1024 . 10) (2048 . 11) (4096 . 12) (8192 . 13) (16384 . 14) (32768 . 15) (65536 . 16) (131072 . 17) (262144 . 18) (524288 . 19) (1048576 . 20) (2097152 . 21) (4194304 . 22) (8388608 . 23) (16777216 . 24) (33554432 . 25) (67108864 . 26) (134217728 . 27) (268435456 . 28) (536870912 . 29) (1073741824 . 30) (2147483648 . 31) (4294967296 . 32) (8589934592 . 33) (17179869184 . 34) (34359738368 . 35) (68719476736 . 36)
                    (#x2000000000 . 37) (#x4000000000 . 38) (#x8000000000 . 39) (#x10000000000 . 40) (#x20000000000 . 41) (#x40000000000 . 42) (#x80000000000 . 43) (#x100000000000 . 44) (#x200000000000 . 45) (#x400000000000 . 46) (#x800000000000 . 47) (#x1000000000000 . 48) (#x2000000000000 . 49) (#x4000000000000 . 50) (#x8000000000000 . 51) (#x10000000000000 . 52) (#x20000000000000 . 53) (#x40000000000000 . 54) (#x80000000000000 . 55) (#x100000000000000 . 56) (#x200000000000000 . 57) (#x400000000000000 . 58) (#x800000000000000 . 59) (#x1000000000000000 . 60))))
      (* (log x) 1.4426950408889634)
      ))


(defun diff² (num1 num2)
  "Squared difference of NUM1 and NUM2"
  (let1 diff (- num1 num2)
    (* diff diff)
    ))



(provide 'shortcuts-math)

;;; math-shortcuts.el ends here
